const express = require('express');
const app = express();
const user_todo_list = require('./routes/user-todo-list');
const user_profile = require('./routes/user-profile');
const auth = require('./auth');
const cors = require('cors');
require("./db/db-connection");
app.use(cors({origin: 'http://localhost:4200'}));
app.use(express.json());


app.use("/api/user/todo", user_todo_list);
app.use("/api/user", user_profile);

const port = process.env.PORT || 3000;
app.listen(port, () => {
    console.log(`Server started on port ${port}`);
})


// var corsOptions = {
//     origin: 'http://localhost:4200'
//   }
//   app.use('*', function(req, res, next) {
//     res.setHeader("Access-Control-Allow-Origin", "http://localhost:4200/"); // update to match the domain you will make the request from
//     res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
//     res.setHeader("Access-Control-Allow-Methods","GET, OPTIONS, POST, PUT");
//     res.setHeader("Access-Control-Allow-Credentials",true);
//     next();
//   });
//   app.use( function(req, res, next) {
//     res.setHeader("Access-Control-Allow-Origin", "http://localhost:4200/"); // update to match the domain you will make the request from
//     res.setHeader("Access-Control-Allow-Headers", "X-Requested-With, Content-Type");
//     res.setHeader("Access-Control-Allow-Methods","GET, OPTIONS, POST, PUT");
//     res.setHeader("Access-Control-Allow-Credentials",true);
//     next();
//   });
// app.use(cors(corsOptions));
// app.use(passport.initialize());
// var authenticateUser = function (req, res, next) {
//     if (req._parsedUrl.pathname === '/api/user/signup' || req._parsedUrl.pathname === '/api/user/login') {
//         next();
//     } else {
//         var tempToken = req.headers.authorization;
//         var token = tempToken.split(" ")
//         // console.log(token);
//         auth.verify(token[1])
//             .then((response) => {
//                 res.locals.user_id = response;
//                 next();
//             })
//             .catch((error) => {
//                res.json({error:error});
//             next();
//             })
//     }
// }

// app.use(authenticateUser);
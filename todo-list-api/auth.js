var jwt = require('jsonwebtoken');
var secret='secretSignatutre'
var sign=function(user_id){
    var token = jwt.sign({exp: Math.floor(Date.now() / 1000) + (3600), user_id: user_id},secret );
    return token;
}
var verify=function(token){
  return new Promise(function(resolve,reject){
    if (!token) {
      reject("Authentication Failed");
    }
    else{
      jwt.verify(token, secret, function(error, res) {
              if (error)
              {
                reject(error);
              }
              else{
                resolve(res.user_id)
              }
      });
    }
  });
}

module.exports = {
   sign: sign,
   verify:verify,
};
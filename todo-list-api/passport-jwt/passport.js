const passport = require("passport");
const passportJWT = require("passport-jwt");
var jwt = require('jsonwebtoken');
const user_account_model = require('../db/models/user-accounts');
var ExtractJwt = passportJWT.ExtractJwt;
var JwtStrategy = passportJWT.Strategy;

var jwtOptions = {}
jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
jwtOptions.secretOrKey = 'mysecretword';

var strategy = new JwtStrategy(jwtOptions, function(jwt_payload, next) {
    user_account_model
        .findOne({ _id: jwt_payload.user_id },(err,user)=>{
          if (err){
            return next(err)
          }
          else if(!user){
            return next(null,false);
          }
          else{
            return next(null,user._id);
            // return next(null,false);
          }
        })
        // .exec((err, user) => next(err,user._id));
  });
passport.use(strategy);

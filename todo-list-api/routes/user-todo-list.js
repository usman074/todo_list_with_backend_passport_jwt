const express = require('express');
const router = express.Router();
const dbQueries = require("../db/dbQueries/todo-list-queries");
const auth = require('../auth');
const user_todo = require('../controller/user-todo-list-controller');
const passport = require('passport');
require('../passport-jwt/passport');
router.get('/getTodo', passport.authenticate('jwt', { session: false }), user_todo.getTodo);
router.get('/getTodo/:id', passport.authenticate('jwt', { session: false }), user_todo.getTodoById);
router.post('/addTodo', passport.authenticate('jwt', { session: false }), user_todo.postTodo);
router.put('/updateTodo', passport.authenticate('jwt', { session: false }), user_todo.updateTodo);
router.delete('/delTodo/:id', passport.authenticate('jwt', { session: false }), user_todo.delTodo);

module.exports = router;












// router.get('/getTodo',passport.authenticate('jwt', { session: false }),(req,res)=>{
//     dbQueries.getTodo(res.locals.user_id)
//     .then((response)=>{
//         res.json({message:response});
//     })
//     .catch((error)=>{
//         res.json({error:error});
//     })
// });



// router.get('/getTodo/:id',(req,res)=>{
//     dbQueries.getTodoById(req.params.id,res.locals.user_id)
//     .then((response)=>{
//         res.json({message:response});
//     })
//     .catch((error)=>{
//         res.json({error:error});
//     })
// });

// router.post('/addTodo',(req,res)=>{
//     dbQueries.postTodo(res.locals.user_id,req.body)
//     .then((response)=>{
//         res.json({message:response});
//     })
//     .catch((error)=>{
//         res.json({error:error});
//     })
// });

// router.put('/updateTodo',(req,res)=>{
//     dbQueries.verifyTask(req.body._id,res.locals.user_id)
//     .then((task)=>{
//         if(task){
//             if(!task.complete)
//             {
//                 dbQueries.updateTaskName(req.body)
//                 .then((response)=>{
//                     res.json({message:response});
//                 })
//                 .catch((error)=>{
//                     res.json({error:error});
//                 })
//             }
//             else{
//                 res.json({error:"Cant update task. Already Completed"});
//             }
//         }
//         else{
//             res.json({error:"Task Not Exist"});
//         }
//     })
//     .catch((error)=>{
//         res.json({error:error});
//     })
// });
// router.delete('/delTodo/:id',(req,res)=>{
//     dbQueries.verifyTask(req.params.id,res.locals.user_id)
//     .then((task)=>{
//         if(task){
//             dbQueries.delTodo(req.params.id)
//             .then(()=>{
//                 res.json({message : "task deleted successfully"});
//             })
//             .catch((error)=>{
//                 res.json({error:error});
//             })
//         }
//         else{
//             res.json({error:"Task Not Exist"});
//         }
//     })
//     .catch((error)=>{
//         res.json({error:error});
//     })

//     // console.log("delete todo");
// });

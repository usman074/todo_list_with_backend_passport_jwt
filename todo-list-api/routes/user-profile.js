const express = require('express');
const router = express.Router();
const dbQueries = require("../db/dbQueries/user-accounts-queries");
const auth = require('../auth');
const jwt = require('jsonwebtoken');
const user_profile = require('../controller/user-profile-controller');


router.post('/login', user_profile.login);
router.post('/signup', user_profile.signup);

module.exports = router;
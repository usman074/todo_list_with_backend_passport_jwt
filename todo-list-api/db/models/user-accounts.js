const mongoose = require("mongoose");

const userAccountSchema = new mongoose.Schema({
    name:String,
    email:String,
    password:String
});

module.exports = mongoose.model("user_profiling",userAccountSchema);
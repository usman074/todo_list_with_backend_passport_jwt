const mongoose = require("mongoose");

const userTodoListSchema = new mongoose.Schema({
    user_id : String,
    name:String,
    complete:Boolean
});

module.exports = mongoose.model("user_todo_list",userTodoListSchema);
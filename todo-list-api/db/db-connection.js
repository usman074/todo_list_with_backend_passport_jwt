const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/todoListApp',{ useNewUrlParser: true, useUnifiedTopology: true } )
    .then(()=> console.log("connected to mongo db"))
    .catch(err => console.log("could not connect to mongo db",err));
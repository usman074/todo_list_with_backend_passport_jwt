const todo_list_model = require('../models/todo-list');

getTodo = (user_id) => {
    
    return new Promise((resolve,reject)=>{
        try{
            // console.log(user_id)
            const tasks =todo_list_model
            .find({ user_id: user_id });
            resolve(tasks);
        }
        catch(error)
        {
            reject(new Error(error));
        }
    })
}

getTodoById = (todoId,user_id) => {
    return new Promise((resolve,reject)=>{
        try{
            const tasks =todo_list_model
            .find({ user_id: user_id, _id : todoId });
            resolve(tasks);
        }
        catch(error)
        {
            reject(new Error(error));
        }
    })
}

postTodo = (user_id,request)=>{
    return new Promise((resolve, reject) => {
        try {
            const addTodo = todo_list_model();
            addTodo.name = request.name;
            addTodo.user_id = user_id;
            addTodo.complete = false;
            addTodo.save((err,task)=>{
                console.log(task)
                resolve(task);
            });
            // console.log(addTodo)
            
        }
        catch (error) {
            reject(new Error(error));
        }
    });
}

verifyTask = (taskId,user_id)=>{
    return new Promise((resolve,reject)=>{

        const task = todo_list_model.findOne({_id:taskId,user_id:user_id});
        if(task){
            resolve(task);
        }
        else{
            reject("error")
        }
    })
}

delTodo = (taskId)=>{
    return new Promise((resolve,reject)=>{
        try{
            todo_list_model.findByIdAndRemove({_id:taskId}, (err, response) => {
                resolve();
            });
        }
        catch(error)
        {
            reject(new Error(error));
        }
    })
}

updateTaskName = (request)=>{
    return new Promise((resolve,reject) =>{
        try{
            const updatedTask = todo_list_model.findByIdAndUpdate(request._id,{
                $set :{
                    name: request.name,
                    complete: request.complete
                }
            },{new : true});
            resolve(updatedTask);
        }
        catch(error){
            reject(error);
        }
    })
}

module.exports = {
    getTodo : getTodo,
    postTodo : postTodo,
    delTodo : delTodo,
    verifyTask : verifyTask,
    updateTaskName : updateTaskName,
    getTodoById :getTodoById
}
const user_account_model = require('../models/user-accounts');

signUp = (request) => {
    return new Promise((resolve, reject) => {
        try {
            const signup = user_account_model();
            signup.name = request.name;
            signup.email = request.email;
            signup.password = request.password;
            signup.save();
            resolve("signup successfully");
        }
        catch (error) {
            reject(new Error(error));
        }
    });
}

login = (request, callback) => {
    user_account_model
        .findOne({ email: request.email })
        .exec((err, user) => callback(err, user));
}

emailValidation = (request,callback) =>{
    user_account_model
    .find({ email: request.email })
    .exec((err, user) => callback(err, user));

}
module.exports = {
    signUp: signUp,
    login: login,
    emailValidation: emailValidation
}
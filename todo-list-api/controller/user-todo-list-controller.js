const dbQueries = require("../db/dbQueries/todo-list-queries");

exports.getTodo = (req,res)=>{
    console.log(req)
    // console.log(req.user)
    dbQueries.getTodo(req.user)
    .then((response)=>{
        // console.log(response);
        res.json({message:response});
    })
    .catch((error)=>{
        res.json({error:error});
    })
}

exports.getTodoById = (req,res)=>{
    dbQueries.getTodoById(req.params.id,req.user)
    .then((response)=>{
        res.json({message:response});
    })
    .catch((error)=>{
        res.json({error:error});
    })
}

exports.postTodo = (req,res)=>{
    dbQueries.postTodo(req.user,req.body)
    .then((response)=>{
        res.json({message:response});
    })
    .catch((error)=>{
        res.json({error:error});
    })
}

exports.updateTodo =(req,res)=>{
    dbQueries.verifyTask(req.body._id,req.user)
    .then((task)=>{
        if(task){
            if(!task.complete)
            {
                dbQueries.updateTaskName(req.body)
                .then((response)=>{
                    res.json({message:response});
                })
                .catch((error)=>{
                    res.json({error:error});
                })
            }
            else{
                res.json({error:"Cant update task. Already Completed"});
            }
        }
        else{
            res.json({error:"Task Not Exist"});
        }
    })
    .catch((error)=>{
        res.json({error:error});
    })
}

exports.delTodo = (req,res)=>{
    dbQueries.verifyTask(req.params.id,req.user)
    .then((task)=>{
        if(task){
            dbQueries.delTodo(req.params.id)
            .then(()=>{
                res.json({message : "task deleted successfully"});
            })
            .catch((error)=>{
                res.json({error:error});
            })
        }
        else{
            res.json({error:"Task Not Exist"});
        }
    })
    .catch((error)=>{
        res.json({error:error});
    })
}
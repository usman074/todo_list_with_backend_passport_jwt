const dbQueries = require("../db/dbQueries/user-accounts-queries");
const jwt = require('jsonwebtoken');
exports.login = (req,res)=>{
    dbQueries.login(req.body, (err, user) => {
        if (err || !user) {
            return res.json({ error: 'User Not Exist' })
        }
        else {
            if (req.body.password === user.password) {
                var payload = {user_id: user._id};
                var token = jwt.sign(payload, 'mysecretword');
                return res.json({ token: token });
            }
            else {
                return res.json({ error: 'Password mismatched' })
            }
        }
    });
}

exports.signup = (req,res)=>{
    dbQueries.emailValidation(req.body, (err, validate) => {
        if (err || validate.length > 0) {
            return res.json({ error: 'Email already registered' });
        }
        else{
            dbQueries.signUp(req.body)
            .then((response) => {
                res.json({message:response});
            })
            .catch((error) => {
                res.json({error:error});
            })
        }
    })
}
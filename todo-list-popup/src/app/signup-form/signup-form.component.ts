import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';
import { AbstractControl } from '@angular/forms';
import { SignupService } from '../../services/signup-service/signup.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-signup-form',
  templateUrl: './signup-form.component.html',
  styleUrls: ['./signup-form.component.css']
})
export class SignupFormComponent implements OnInit {
  
  submitted: boolean = false;
  constructor(private fb: FormBuilder, private signupService : SignupService) { }
  private profileForm: FormGroup;
  ngOnInit() {
    this.profileForm = this.fb.group({
      name: ['', [Validators.required, Validators.minLength(3)]],
      email: ['', [Validators.required/*, this.emailDomainValidator*/]],
      password : ['',[Validators.required, Validators.minLength(6)]]
      // Swal.fire("hello");
    });
  }
  onSubmit() {
    // TODO: Use EventEmitter with form value
    this.submitted = true;
    if (this.profileForm.invalid) {
      return;
    }
    else {
      this.signupService.signUp_user(this.profileForm.value)
      .subscribe(
        data =>{
          // console.log(data);
          this.handleData(data);
        }
      )
    }
  }
  get form() {
    return this.profileForm.controls;
  }
  emailDomainValidator(control: AbstractControl): { [key: string]: any } | null {
    const email: string = control.value;
    const domain = email.substring(email.lastIndexOf('@') + 1);
    if (email === "" || domain === 'gmail.com') {
      return null;
    }
    else {
      return { "domain": true };
    }
  }

  handleData(data)
  {
    if(data.error)
    {
      Swal.fire("Error!",data.error, "error");
    }
    else if(data.message)
    {
      Swal.fire("Success", data.message, "success");
    }
  }
}

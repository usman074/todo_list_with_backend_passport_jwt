import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/authentication-service/auth.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-navbar-logout',
  templateUrl: './navbar-logout.component.html',
  styleUrls: ['./navbar-logout.component.css']
})
export class NavbarLogoutComponent implements OnInit {

  constructor( private authService :AuthService, private router: Router) { }
  title : string = "Todo List Application";
  ngOnInit() {
  }

  logout() : void{
    this.authService.resetToken();
    this.router.navigateByUrl('/login');
  }
}

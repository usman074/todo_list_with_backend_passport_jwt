import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';
import { AbstractControl } from '@angular/forms';
import { LoginService } from '../../services/login-service/login.service';
import { AuthService } from '../../services/authentication-service/auth.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {
  submitted: boolean = false;
  constructor(private fb: FormBuilder, private loginService : LoginService, private authService : AuthService, private router: Router) { }
  private profileForm: FormGroup;

  ngOnInit() {
    this.profileForm = this.fb.group({
      email: ['', [Validators.required, /*this.emailDomainValidator*/]],
      password : ['',[Validators.required, Validators.minLength(6)]]
    });
  }

  onSubmit() {
    // TODO: Use EventEmitter with form value
    this.submitted = true;
    if (this.profileForm.invalid) {
      return;
    }
    else {
      // console.warn(this.profileForm.value);
      this.loginService.login_user(this.profileForm.value)
      .subscribe(
        data => {
          console.log(data);
          this.handleData(data);
        }
      )
    }
  }
  get form() {
    return this.profileForm.controls;
  }
  emailDomainValidator(control: AbstractControl): { [key: string]: any } | null {
    const email: string = control.value;
    const domain = email.substring(email.lastIndexOf('@') + 1);
    if (email === "" || domain === 'gmail.com') {
      return null;
    }
    else {
      return { "domain": true };
    }
  }

  handleData(data)
  {
    if(data.error)
    {
      Swal.fire("Error!",data.error, "error");
    }
    else if(data.token){
      console.log(data)
      this.authService.setToken(data.token);
      this.router.navigateByUrl('/todoList');
    }
  }
}

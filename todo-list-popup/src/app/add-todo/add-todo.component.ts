import { Component, OnInit } from '@angular/core';
import { TaskListService } from '../task-list.service';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { TodoListService } from '../../services/todo-list-service/todo-list.service';
import { LocalDataService } from '../../services/local-data-service/local-data.service';
import { taskDataFormat } from '../todo-list/task-Data-Format';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-add-todo',
  templateUrl: './add-todo.component.html',
  styleUrls: ['./add-todo.component.css']
})
export class AddTodoComponent implements OnInit {

  constructor(private taskList : TaskListService, private location : Location, private route: ActivatedRoute, private router: Router,private todoListService : TodoListService, private localDataService : LocalDataService) { }
  taskValue : taskDataFormat=null;
  taskName : string = "";
  btn_name : string;
  ngOnInit() {
    if(this.route.snapshot.url[0].path === "addTask")
    {
      this.btn_name = "Save";
    }
    else if(this.route.snapshot.url[0].path === "updateTask"){
      this.btn_name = "Update";
      this.getTaskValue();
    }
  }
  getTaskValue(): void {
    const id = this.route.snapshot.paramMap.get('id');
    if(id){
      this.todoListService.getTask(id)
      .subscribe(data =>{
        if(data.error)
        {

        }
        else if(data.message){
          this.taskValue = data.message[0];
          this.taskName = this.taskValue.name;
        }
      })
    }
    
  }

  savetask(taskName : string) : void{
    // if(taskName.length > 0){

      if(this.btn_name === "Save")
    
    {
      this.todoListService.addTask(taskName)
      .subscribe(data =>{
        this.localDataService.saveTask(data);
      })
    }
    else if(this.btn_name === "Update"){
      this.taskValue.name = taskName;
      this.todoListService.updateTask(this.taskValue)
      .subscribe(data => {
        const index = this.route.snapshot.paramMap.get('index');
        this.localDataService.updateTask(data,index);
      })
      this.btn_name = "Save"
    }
    this.router.navigateByUrl('/todoList');
  // }
  // else if(taskName.length == 0){
  //   Swal.fire("Error!","Task Name cant be empty", "error");
  // }
}
  goBack(): void {
    this.location.back();
  }

}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TodoListComponent } from './todo-list/todo-list.component';
import { ModalComponent } from './modal/modal.component';
import { InputFieldComponent } from './input-field/input-field.component';
import { AddTodoComponent } from './add-todo/add-todo.component';
import { NavbarComponent } from './navbar/navbar.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { SignupFormComponent } from './signup-form/signup-form.component';
import { HttpClientModule, HTTP_INTERCEPTORS }    from '@angular/common/http';
import { NavbarLogoutComponent } from './navbar-logout/navbar-logout.component';
import { TokenInterceptorService } from '../services/token-interceptor-service/token-interceptor.service';

@NgModule({
  declarations: [
    AppComponent,
    TodoListComponent,
    ModalComponent,
    InputFieldComponent,
    AddTodoComponent,
    NavbarComponent,
    LoginFormComponent,
    SignupFormComponent,
    NavbarLogoutComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
    
  ],
  providers: [{
    provide : HTTP_INTERCEPTORS,
    useClass : TokenInterceptorService,
    multi : true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }

export class taskDataFormat {
    _id : string;
    name : string;
    user_id : string;
    complete : boolean;
}
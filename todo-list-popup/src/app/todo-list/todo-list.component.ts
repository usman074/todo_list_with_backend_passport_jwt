import { Component, OnInit } from '@angular/core';
import { TaskListService } from '../task-list.service';
// import { taskStructure } from '../taskStructure';
import { AuthService } from '../../services/authentication-service/auth.service';
import { TodoListService } from '../../services/todo-list-service/todo-list.service';
import { taskDataFormat } from './task-Data-Format';
import { Router } from '@angular/router';
import { LocalDataService } from '../../services/local-data-service/local-data.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent implements OnInit {

  constructor(private taskList : TaskListService, private authService : AuthService, private router: Router,private todoListService : TodoListService, private localDataService : LocalDataService) { }

  tasks : taskDataFormat[] = [];
  modalTitle:string;
  modalButtonName : string;
  // selectedTask : taskStructure;
  taskValue: string;
  ngOnInit() {
    
    console.log(this.tasks.length);
    if(this.tasks.length === 0){
      if(this.authService.getToken())
      {
        this.todoListService.getTaskList()
        .subscribe(data => this.handleData(data), error => this.handleError(error));
      }
      else{
        this.router.navigateByUrl('/signup');
      }
    }
    else{
      if(this.localDataService.getTask() != null)
      {
        this.tasks.push(this.localDataService.getTask());
      }
      if(this.localDataService.getUpdateTask() != null){
        this.tasks[this.localDataService.getIndex()] = this.localDataService.getUpdateTask();
      }
    }
  }

  handleData(data) : void{
    if(data.error)
    {
      Swal.fire("Error!",data.error, "error");
    }
    else if(data.message)
    {
      console.log("inside")
      console.log(data.message);
      this.tasks = data.message;
    }
  }
  handleError(error) : void{
    if(error.status === 401){
      Swal.fire("Error!",error.statusText, "error");
      this.router.navigateByUrl('/login');
    }
    else {
      Swal.fire("Error!",error.statusText, "error");
    }
  }
  delTask(task : taskDataFormat,index : number) : void{
    // this.tasks = this.taskList.delTask(id);
    this.todoListService.delTask(task._id)
    .subscribe(data=>{
      // console.log(data);
      // this.ngOnInit();
      console.log(index)
      // if(this.tasks.length == 1){
      //   this.tasks = null;
      // }
      // else{

        
      // }
      this.tasks.splice(index,1);
      console.log(this.tasks)
    }, error => this.handleError(error))
  }
  onCompleted(task : taskDataFormat) : void{
    task.complete = true;
    this.todoListService.updateTask(task)
    .subscribe(data => {
      // if(data.error){
      //   Swal.fire("Error!",data.error, "error");
      // }
    }, error => this.handleError(error));
  }

  // setModal(btn_click : string, task : taskStructure) : void {
  //   if(btn_click === 'addTask')
  //   {
  //     this.modalTitle = "Add Task";
  //     this.modalButtonName = "Save";
  //     this.taskValue = null;
  //   }
  //   else if(btn_click === 'editTask')
  //   {
  //     this.modalTitle = "Update Task";
  //     this.modalButtonName = "Update";
  //     this.selectedTask = task;
  //     this.taskValue = task.name;
  //   }
  // }

  // savetask(taskName : string) : void{
  //   if(this.modalButtonName === "Save")
  //   {
  //     this.taskList.addTask(taskName);
  //   }
  //   else if(this.modalButtonName === "Update"){
  //     this.taskList.updateTaskName(this.selectedTask.id,taskName);
  //     this.modalButtonName = "Save"
  //   }
  // }
}

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-input-field',
  templateUrl: './input-field.component.html',
  styleUrls: ['./input-field.component.css']
})
export class InputFieldComponent implements OnInit {

  @Input() btn_name : string;
  @Input() taskValue : string;
  @Output() taskName = new EventEmitter<string>();
  constructor() { }

  ngOnInit() {
  }

  sendTaskNameToParent(taskName : string){
    this.taskName.emit(taskName);
  }
}

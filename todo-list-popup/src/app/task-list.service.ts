import { Injectable } from '@angular/core';
import { taskStructure } from './taskStructure';
@Injectable({
  providedIn: 'root'
})
export class TaskListService {

  constructor() { }
  tasks : taskStructure[] =[];
    taskObj : taskStructure;
    id : number;
    addTask(taskName : string) : taskStructure{
        // console.log(taskName);
         this.id = this.genId();
         this.taskObj= {
            id : this.id,
            name : taskName,
            complete : false
        }
        this.tasks.push(this.taskObj)
        return this.taskObj;
    }

    delTask(id : number) : taskStructure[]
    {
      for(var i =0; i < this.tasks.length; i++)
      {
        if(this.tasks[i].id === id)
        {
          this.tasks.splice(i,1);
          break;
        }
      }
      return this.tasks;
    }

    updateTask(id : number) : taskStructure[]
    {
      for(var i =0; i < this.tasks.length; i++)
      {
        if(this.tasks[i].id === id)
        {
          this.tasks[i].complete = true;
          break;
        }
      }
      return this.tasks;
    }

    updateTaskName(id : number, taskName : string)
    {
      for(var i =0; i < this.tasks.length; i++)
      {
        if(this.tasks[i].id === id)
        {
          this.tasks[i].name = taskName;
          break;
        }
      }
    }

    getTask(id : number) : string{
      for(var i =0; i < this.tasks.length; i++)
      {
        if(this.tasks[i].id === id)
        {
          return this.tasks[i].name;
        }
      }
      return null;
    }

    genId(): number{
        return this.tasks.length > 0 ? Math.max(...this.tasks.map(task => task.id)) + 1 : 1;
    }

    getTaskList() :  taskStructure[]{
      return this.tasks;
    }
}

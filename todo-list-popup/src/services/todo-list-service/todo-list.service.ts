import { Injectable } from '@angular/core';
import { AuthService } from '../authentication-service/auth.service';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { todoListDataFormat } from './todo-list-data-format';
import { catchError } from 'rxjs/operators'
@Injectable({
  providedIn: 'root'
})
export class TodoListService {
  taskObj : todoListDataFormat;
  constructor(private authService : AuthService, private http: HttpClient) { }
  token = this.authService.getToken();
  httpOptions = {
    headers: new HttpHeaders({ 'x-access-token': this.token })
  };
  private getTodoUrl = 'http://localhost:3000/api/user/todo/getTodo';
  private addTodoUrl = 'http://localhost:3000/api/user/todo/addTodo';
  private updateTodoUrl = 'http://localhost:3000/api/user/todo/updateTodo';
  private delTodoUrl = 'http://localhost:3000/api/user/todo/delTodo';
  getTaskList(): Observable<any>{
    return this.http.get(this.getTodoUrl)
    .pipe(
      catchError(this.handleError)
      );
  }
  addTask(data : string) : Observable<any>{
    console.log("add")
      this.taskObj = {
        name : data,
      }
    return this.http.post(this.addTodoUrl,this.taskObj)
    .pipe(
      catchError(this.handleError)
      );
  }
  updateTask(data) : Observable<any> {
    return this.http.put(this.updateTodoUrl,data)
    .pipe(
      catchError(this.handleError)
      );
  }
  delTask(id) : Observable<any>{
    return this.http.delete(this.delTodoUrl+"/"+id)
    .pipe(
      catchError(this.handleError)
      );
  }
  getTask(id : string) : Observable<any>{
    return this.http.get(this.getTodoUrl+"/"+id)
    .pipe(
      catchError(this.handleError)
      );
  }
  handleError(error: HttpErrorResponse){
    return throwError(error);
    }
}

import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalDataService {

  constructor() { }

  data:any;
  update_task : any;
  index:number;
  saveTask(data) : void{
    this.data= data;
  }
  getTask():any{
    return this.data;
  }
  updateTask(data,ind):void{
    this.update_task = data
    this.index = ind;
  }
  getUpdateTask():any{
    return this.update_task;
  }
  getIndex():number{
    return this.index;
  }
}

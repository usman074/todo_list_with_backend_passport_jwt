import { Injectable } from '@angular/core';
import { loginDataFormat } from './login-data-format';
import { HttpClient, HttpHeaders, HttpErrorResponse  } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators'
import { throwError } from 'rxjs';
// import 'rxjs/add/operator/catch';
// import 'rxjs/';
@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private loginUrl = 'http://localhost:3000/api/user/login';
  constructor(private http: HttpClient) { }
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  login_user(data: loginDataFormat): Observable<any> {
    return this.http.post(this.loginUrl, data);    
  }
}

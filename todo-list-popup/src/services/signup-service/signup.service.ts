import { Injectable } from '@angular/core';
import { signupDataFormat } from './signup-data-format';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class SignupService {
  private signUpurl = 'http://localhost:3000/api/user/signup';
  // tempdata = {
  //   "title" :"hello",
  //   "body" : "hi",
  //   "userId" : 1
  // }
  constructor(private http: HttpClient) { }
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  signUp_user(data : signupDataFormat) : Observable<any> {
    return this.http.post(this.signUpurl, data);
  }
}

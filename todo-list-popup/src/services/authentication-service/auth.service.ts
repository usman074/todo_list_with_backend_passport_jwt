import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  // authToken : string=null;

  setToken(authToken : string){
    // this.authToken = authToken;
    localStorage.setItem("token",authToken);
    // console.log("usman",localStorage.getItem("token"));
  }
  getToken() : string{
    return localStorage.getItem("token");
  }

  resetToken() : void{
    localStorage.removeItem("token");
  }
}
